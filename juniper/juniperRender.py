#!/usr/bin/env python3
from jinja2 import Environment, FileSystemLoader
import  sys, os, yaml, napalm, argparse, datetime, configparser

def parser():
    parser = argparse.ArgumentParser(description="""
    Juniper Configurator. YAML file is used for config variableshost. JINJA files
    are rendered and pushed to one or more devices via NAPALM- Version 1.1.f180628""")
    parser.add_argument('--device', '-d', dest='device', help="Target host",type=str)
    parser.add_argument('--inventory', '-i', dest='inventory', help="inventory file",type=str)
    args = parser.parse_args()
    device = args.device
    inventory = args.inventory
    # read INI inventory
    config = configparser.ConfigParser(allow_no_value=True)
    config.read(inventory)
    if inventory == None:
        hostList = []
        hostList.append(device)
        return hostList
    else:
        for host in config.sections():
            return config.options(host)

def render(device):
    ENV = Environment(loader=FileSystemLoader('.'))
    configFileList = []
    for host in device:
        template = ENV.get_template(str(host) + '/config.j2')
        yamlVar = {}
        # update dic with all.yaml
        with open('all.yaml', 'r') as all:
            yamlAll = yaml.load(all)
            yamlVar.update(yamlAll)
        # walk through all yaml files
        for root, dirpath, filenames in os.walk(os.getcwd()):
            if host in root:
                for file in filenames:
                    if '.yaml' in file or '.yml' in file:
                        yamlFile = yaml.load(open(os.path.join(root, file)))
                        yamlVar = {**yamlVar, **yamlFile}
        # create config file
        render = template.render(yamlVar=yamlVar)
        configFile ='cfgs/' + host  + '-' + str(datetime.datetime.now().isoformat())
        configFileList.append(configFile)
        with open(configFile, 'w+') as configTest:
            configTest.write(render)
            configTest.close()
    return configFileList

def loadReplace(config,device):
    for host in device:
        print('Loading config file for {}\n'.format(host.upper()))
        driver = napalm.get_network_driver('junos')
        device = driver(hostname=host, username='olivierif', password='')
        print('Opening ssh session to {}\n'.format(host.upper()))
        device.open()
        device.load_replace_candidate(filename=cfg)
        print('\nDiff:')
        print(device.compare_config())
        try:
            choice = input("\nWould you like to commit these changes? [yN]: ")
        except NameError:
            choice = input("\nWould you like to commit these changes? [yN]: ")
        if choice == 'y':
            print('Committing ...')
            device.commit_config()
        else:
            print('Discarding ...')
            device.discard_config()
            os.remove(cfg)
        device.close()
        print('Done.\n')

def main():
    loadReplace(render(parser()),parser())

if __name__ == "__main__":
    main()
